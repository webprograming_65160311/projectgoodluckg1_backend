import { PartialType } from '@nestjs/swagger';
import { CreateUtilityBillDto } from './create-utility-bill.dto';

export class UpdateUtilityBillDto extends PartialType(CreateUtilityBillDto) {}
