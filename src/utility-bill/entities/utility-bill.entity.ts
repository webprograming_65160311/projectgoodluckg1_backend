import { TypeUtilityBill } from 'src/type-utility-bill/entities/type-utility-bill.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class UtilityBill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  invoiceNumber: string;

  @Column()
  date: Date;

  @Column()
  amount: number;

  @Column({ nullable: true })
  remark: string;

  @Column()
  status: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => User, (user) => user.utilityBill, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  user: User;

  @ManyToOne(() => TypeUtilityBill, (type) => type.utilityBills, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  type: TypeUtilityBill;
}
