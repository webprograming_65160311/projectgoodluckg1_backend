import { Injectable } from '@nestjs/common';
import { CreateUtilityBillDto } from './dto/create-utility-bill.dto';
import { UpdateUtilityBillDto } from './dto/update-utility-bill.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { UtilityBill } from './entities/utility-bill.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UtilityBillService {
  constructor(
    @InjectRepository(UtilityBill)
    private utilityBillRepository: Repository<UtilityBill>,
  ) {}
  create(createUtilityBillDto: UpdateUtilityBillDto) {
    return this.utilityBillRepository.save(createUtilityBillDto);
  }

  findAll() {
    return this.utilityBillRepository.find({
      relations: { user: true, type: true },
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} utilityBill`;
  }

  update(id: number, updateUtilityBillDto: UpdateUtilityBillDto) {
    return `This action updates a #${id} utilityBill`;
  }

  remove(id: number) {
    return `This action removes a #${id} utilityBill`;
  }
}
