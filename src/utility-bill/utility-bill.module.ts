import { Module } from '@nestjs/common';
import { UtilityBillService } from './utility-bill.service';
import { UtilityBillController } from './utility-bill.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UtilityBill } from './entities/utility-bill.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UtilityBill])],
  controllers: [UtilityBillController],
  providers: [UtilityBillService],
  exports: [UtilityBillService],
})
export class UtilityBillModule {}
