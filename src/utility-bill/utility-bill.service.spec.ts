import { Test, TestingModule } from '@nestjs/testing';
import { UtilityBillService } from './utility-bill.service';

describe('UtilityBillService', () => {
  let service: UtilityBillService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UtilityBillService],
    }).compile();

    service = module.get<UtilityBillService>(UtilityBillService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
