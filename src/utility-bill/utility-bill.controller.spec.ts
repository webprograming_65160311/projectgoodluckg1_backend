import { Test, TestingModule } from '@nestjs/testing';
import { UtilityBillController } from './utility-bill.controller';
import { UtilityBillService } from './utility-bill.service';

describe('UtilityBillController', () => {
  let controller: UtilityBillController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UtilityBillController],
      providers: [UtilityBillService],
    }).compile();

    controller = module.get<UtilityBillController>(UtilityBillController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
