import { Module } from '@nestjs/common';
import { TypeUtilityBillService } from './type-utility-bill.service';
import { TypeUtilityBillController } from './type-utility-bill.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeUtilityBill } from './entities/type-utility-bill.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TypeUtilityBill])],
  controllers: [TypeUtilityBillController],
  providers: [TypeUtilityBillService],
  exports: [TypeUtilityBillService],
})
export class TypeUtilityBillModule {}
