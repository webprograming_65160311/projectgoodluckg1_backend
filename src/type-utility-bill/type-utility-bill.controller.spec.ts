import { Test, TestingModule } from '@nestjs/testing';
import { TypeUtilityBillController } from './type-utility-bill.controller';
import { TypeUtilityBillService } from './type-utility-bill.service';

describe('TypeUtilityBillController', () => {
  let controller: TypeUtilityBillController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TypeUtilityBillController],
      providers: [TypeUtilityBillService],
    }).compile();

    controller = module.get<TypeUtilityBillController>(
      TypeUtilityBillController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
