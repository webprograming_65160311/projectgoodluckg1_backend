import { Injectable } from '@nestjs/common';
import { UpdateTypeUtilityBillDto } from './dto/update-type-utility-bill.dto';
import { TypeUtilityBill } from './entities/type-utility-bill.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTypeUtilityBillDto } from './dto/create-type-utility-bill.dto';

@Injectable()
export class TypeUtilityBillService {
  constructor(
    @InjectRepository(TypeUtilityBill)
    private typeUtilityBillRepository: Repository<TypeUtilityBill>,
  ) {}
  create(createTypeUtilityBill: CreateTypeUtilityBillDto) {
    return this.typeUtilityBillRepository.save(createTypeUtilityBill);
  }

  findAll() {
    return this.typeUtilityBillRepository.find();
  }
  findOne(id: number) {
    return this.typeUtilityBillRepository.findOne({ where: { id } });
  }

  update(id: number, updateTypeUtilityBillDto: UpdateTypeUtilityBillDto) {
    return `This action updates a #${id} typeUtilityBill`;
  }

  async remove(id: number) {
    const removedType = await this.typeUtilityBillRepository.findOneByOrFail({
      id,
    });
    return this.typeUtilityBillRepository.remove(removedType);
  }
}
