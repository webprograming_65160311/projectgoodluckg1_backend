import { Test, TestingModule } from '@nestjs/testing';
import { TypeUtilityBillService } from './type-utility-bill.service';

describe('TypeUtilityBillService', () => {
  let service: TypeUtilityBillService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TypeUtilityBillService],
    }).compile();

    service = module.get<TypeUtilityBillService>(TypeUtilityBillService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
