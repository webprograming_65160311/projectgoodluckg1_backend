import { PartialType } from '@nestjs/swagger';
import { CreateTypeUtilityBillDto } from './create-type-utility-bill.dto';

export class UpdateTypeUtilityBillDto extends PartialType(
  CreateTypeUtilityBillDto,
) {}
