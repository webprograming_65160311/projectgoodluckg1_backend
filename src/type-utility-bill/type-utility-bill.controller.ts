import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TypeUtilityBillService } from './type-utility-bill.service';
import { CreateTypeUtilityBillDto } from './dto/create-type-utility-bill.dto';
import { UpdateTypeUtilityBillDto } from './dto/update-type-utility-bill.dto';

@Controller('type-utility-bill')
export class TypeUtilityBillController {
  constructor(
    private readonly typeUtilityBillService: TypeUtilityBillService,
  ) {}

  @Post()
  create(@Body() createTypeUtilityBillDto: CreateTypeUtilityBillDto) {
    return this.typeUtilityBillService.create(createTypeUtilityBillDto);
  }

  @Get()
  findAll() {
    return this.typeUtilityBillService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.typeUtilityBillService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTypeUtilityBillDto: UpdateTypeUtilityBillDto,
  ) {
    return this.typeUtilityBillService.update(+id, updateTypeUtilityBillDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.typeUtilityBillService.remove(+id);
  }
}
