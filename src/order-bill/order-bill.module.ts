import { Module } from '@nestjs/common';
import { OrderBillService } from './order-bill.service';
import { OrderBillController } from './order-bill.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderBill } from './entities/order-bill.entity';

@Module({
  imports: [TypeOrmModule.forFeature([OrderBill])],
  controllers: [OrderBillController],
  providers: [OrderBillService],
})
export class OrderBillModule {}
