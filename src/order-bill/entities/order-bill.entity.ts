import { OrderBillDetail } from 'src/order-bill-detail/entities/order-bill-detail.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class OrderBill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: Date;

  @Column()
  store: string;

  @Column()
  totalPrice: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => OrderBillDetail, (detail) => detail.orderBill)
  orderBillDetails: OrderBillDetail[];

  @ManyToOne(() => User, (user) => user.orderBill, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  user: User;
}
