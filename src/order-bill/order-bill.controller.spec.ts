import { Test, TestingModule } from '@nestjs/testing';
import { OrderBillController } from './order-bill.controller';
import { OrderBillService } from './order-bill.service';

describe('OrderBillController', () => {
  let controller: OrderBillController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrderBillController],
      providers: [OrderBillService],
    }).compile();

    controller = module.get<OrderBillController>(OrderBillController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
