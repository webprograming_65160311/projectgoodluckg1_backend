import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrderBillService } from './order-bill.service';
import { CreateOrderBillDto } from './dto/create-order-bill.dto';
import { UpdateOrderBillDto } from './dto/update-order-bill.dto';

@Controller('order-bill')
export class OrderBillController {
  constructor(private readonly orderBillService: OrderBillService) {}

  @Post()
  create(@Body() createOrderBillDto: CreateOrderBillDto) {
    return this.orderBillService.create(createOrderBillDto);
  }

  @Get()
  findAll() {
    return this.orderBillService.findAll();
  }

  @Get('/last-id')
  async getLastId(): Promise<{ lastId: number | undefined }> {
    const lastId = await this.orderBillService.getLastId();
    return { lastId };
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orderBillService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateOrderBillDto: UpdateOrderBillDto,
  ) {
    return this.orderBillService.update(+id, updateOrderBillDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderBillService.remove(+id);
  }
}
