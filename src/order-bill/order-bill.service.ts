import { Injectable } from '@nestjs/common';
import { CreateOrderBillDto } from './dto/create-order-bill.dto';
import { UpdateOrderBillDto } from './dto/update-order-bill.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderBill } from './entities/order-bill.entity';
import { Repository } from 'typeorm';

@Injectable()
export class OrderBillService {
  constructor(
    @InjectRepository(OrderBill)
    private orderBillRepository: Repository<OrderBill>,
  ) {}

  create(createOrderBillDto: CreateOrderBillDto) {
    return this.orderBillRepository.save(createOrderBillDto);
  }

  findAll() {
    return this.orderBillRepository.find({
      relations: [
        'orderBillDetails',
        'orderBillDetails.rawMaterial',
        'user',
        'user.branch',
      ],
    });
  }

  async getLastId(): Promise<number | undefined> {
    const [latestOrderBill] = await this.orderBillRepository.find({
      order: { id: 'DESC' },
      take: 1,
    });

    return latestOrderBill?.id;
  }

  findOne(id: number) {
    return `This action returns a #${id} orderBill`;
  }

  update(id: number, updateOrderBillDto: UpdateOrderBillDto) {
    return `This action updates a #${id} orderBill`;
  }

  remove(id: number) {
    return `This action removes a #${id} orderBill`;
  }
}
