import { PartialType } from '@nestjs/swagger';
import { CreateOrderBillDto } from './create-order-bill.dto';

export class UpdateOrderBillDto extends PartialType(CreateOrderBillDto) {}
