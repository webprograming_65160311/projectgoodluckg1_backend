import { PartialType } from '@nestjs/swagger';
import { CreateCheckStockDto } from './create-check-stock.dto';

export class UpdateCheckStockDto extends PartialType(CreateCheckStockDto) {}
