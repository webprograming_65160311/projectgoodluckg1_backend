import { CheckRawMaterialDetail } from 'src/check-raw-material-detail/entities/check-raw-material-detail.entity';
import { RawMaterial } from 'src/raw-material/entities/raw-material.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class CheckStock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  checkDate: Date;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => CheckRawMaterialDetail, (detail) => detail.checkStock)
  checkRawMaterialDetails: CheckRawMaterialDetail[];

  @ManyToOne(() => User, (user) => user.checkStock, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  user: User;
}
