import { RawMaterial } from 'src/raw-material/entities/raw-material.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { ReceiptItem } from 'src/receipts/entities/receiptItem.entity';
import { Stock } from 'src/stock/entities/stock.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => User, (user) => user.branch)
  users: User[];

  @OneToMany(() => Stock, (stock) => stock.branch)
  stocks: Stock[];

  @OneToMany(() => Receipt, (receipt) => receipt.branch)
  receipts: Receipt[];
}
