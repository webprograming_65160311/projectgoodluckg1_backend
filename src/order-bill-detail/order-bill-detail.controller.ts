import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrderBillDetailService } from './order-bill-detail.service';
import { CreateOrderBillDetailDto } from './dto/create-order-bill-detail.dto';
import { UpdateOrderBillDetailDto } from './dto/update-order-bill-detail.dto';

@Controller('order-bill-detail')
export class OrderBillDetailController {
  constructor(
    private readonly orderBillDetailService: OrderBillDetailService,
  ) {}

  @Post()
  create(@Body() createOrderBillDetailDto: CreateOrderBillDetailDto) {
    return this.orderBillDetailService.create(createOrderBillDetailDto);
  }

  @Get()
  findAll() {
    return this.orderBillDetailService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orderBillDetailService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateOrderBillDetailDto: UpdateOrderBillDetailDto,
  ) {
    return this.orderBillDetailService.update(+id, updateOrderBillDetailDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderBillDetailService.remove(+id);
  }
}
