import { Injectable } from '@nestjs/common';
import { CreateOrderBillDetailDto } from './dto/create-order-bill-detail.dto';
import { UpdateOrderBillDetailDto } from './dto/update-order-bill-detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderBillDetail } from './entities/order-bill-detail.entity';
import { Repository } from 'typeorm';

@Injectable()
export class OrderBillDetailService {
  constructor(
    @InjectRepository(OrderBillDetail)
    private orderBillDetailRepository: Repository<OrderBillDetail>,
  ) {}

  create(createOrderBillDetailDto: CreateOrderBillDetailDto) {
    return this.orderBillDetailRepository.save(createOrderBillDetailDto);
  }

  findAll() {
    return this.orderBillDetailRepository.find({
      relations: { orderBill: true, rawMaterial: true },
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} orderBillDetail`;
  }

  update(id: number, updateOrderBillDetailDto: UpdateOrderBillDetailDto) {
    return `This action updates a #${id} orderBillDetail`;
  }

  remove(id: number) {
    return `This action removes a #${id} orderBillDetail`;
  }
}
