import { OrderBill } from 'src/order-bill/entities/order-bill.entity';
import { RawMaterial } from 'src/raw-material/entities/raw-material.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class OrderBillDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  quantity: number;

  @Column()
  unitPrice: number;

  @Column()
  totalPrice: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(
    () => RawMaterial,
    (rawMaterial) => rawMaterial.checkRawMaterialDetails,
    { onDelete: 'CASCADE' },
  )
  rawMaterial: RawMaterial;

  @ManyToOne(() => OrderBill, (orderBill) => orderBill.orderBillDetails, {
    onDelete: 'CASCADE',
  })
  orderBill: OrderBill;
}
