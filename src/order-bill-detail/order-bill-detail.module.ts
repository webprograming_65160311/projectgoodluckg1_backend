import { Module } from '@nestjs/common';
import { OrderBillDetailService } from './order-bill-detail.service';
import { OrderBillDetailController } from './order-bill-detail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderBillDetail } from './entities/order-bill-detail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([OrderBillDetail])],
  controllers: [OrderBillDetailController],
  providers: [OrderBillDetailService],
})
export class OrderBillDetailModule {}
