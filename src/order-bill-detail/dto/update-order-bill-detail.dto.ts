import { PartialType } from '@nestjs/swagger';
import { CreateOrderBillDetailDto } from './create-order-bill-detail.dto';

export class UpdateOrderBillDetailDto extends PartialType(
  CreateOrderBillDetailDto,
) {}
