import { Test, TestingModule } from '@nestjs/testing';
import { OrderBillDetailService } from './order-bill-detail.service';

describe('OrderBillDetailService', () => {
  let service: OrderBillDetailService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OrderBillDetailService],
    }).compile();

    service = module.get<OrderBillDetailService>(OrderBillDetailService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
