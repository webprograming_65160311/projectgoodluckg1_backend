import { Test, TestingModule } from '@nestjs/testing';
import { OrderBillDetailController } from './order-bill-detail.controller';
import { OrderBillDetailService } from './order-bill-detail.service';

describe('OrderBillDetailController', () => {
  let controller: OrderBillDetailController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrderBillDetailController],
      providers: [OrderBillDetailService],
    }).compile();

    controller = module.get<OrderBillDetailController>(
      OrderBillDetailController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
