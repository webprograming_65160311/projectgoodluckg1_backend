import { Branch } from 'src/branches/entities/branch.entity';

export class CreateUserDto {
  fullName: string;
  email: string;
  password: string;
  salaryRate: string;
  gender: string;
  image: string;
  role: string;
  branch: string;
}
