import { Branch } from 'src/branches/entities/branch.entity';
import { CheckStock } from 'src/check-stock/entities/check-stock.entity';
import { OrderBill } from 'src/order-bill/entities/order-bill.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Role } from 'src/roles/entities/role.entity';
import { Salary } from 'src/salary/entities/salary.entity';
import { TimeAttendance } from 'src/time-attendance/entities/time-attendance.entity';
import { UtilityBill } from 'src/utility-bill/entities/utility-bill.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fullName: string;

  @Column()
  email: string;

  @Column({ select: false })
  password: string;

  @Column()
  gender: string;

  @Column({ default: 12000 })
  salaryRate: number;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Role, (role) => role.users, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  role: Role;

  @ManyToOne(() => Branch, (branch) => branch.users, { nullable: true })
  branch: Branch;

  @OneToMany(() => Receipt, (receipt) => receipt.user)
  receipts: Receipt[];

  @OneToMany(() => CheckStock, (checkStock) => checkStock.user)
  checkStock: CheckStock[];

  @OneToMany(() => OrderBill, (orderBill) => orderBill.user)
  orderBill: OrderBill[];

  @OneToMany(() => UtilityBill, (utilityBill) => utilityBill.user)
  utilityBill: UtilityBill[];

  @OneToMany(() => TimeAttendance, (ta) => ta.user)
  timeAttendances: TimeAttendance[];

  @OneToMany(() => Salary, (salary) => salary.user)
  salary: Salary[];
}
