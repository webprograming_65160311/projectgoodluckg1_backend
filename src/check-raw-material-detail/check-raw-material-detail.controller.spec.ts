import { Test, TestingModule } from '@nestjs/testing';
import { CheckRawMaterialDetailController } from './check-raw-material-detail.controller';
import { CheckRawMaterialDetailService } from './check-raw-material-detail.service';

describe('CheckRawMaterialDetailController', () => {
  let controller: CheckRawMaterialDetailController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckRawMaterialDetailController],
      providers: [CheckRawMaterialDetailService],
    }).compile();

    controller = module.get<CheckRawMaterialDetailController>(
      CheckRawMaterialDetailController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
