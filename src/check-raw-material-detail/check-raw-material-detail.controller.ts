import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CheckRawMaterialDetailService } from './check-raw-material-detail.service';
import { CreateCheckRawMaterialDetailDto } from './dto/create-check-raw-material-detail.dto';
import { UpdateCheckRawMaterialDetailDto } from './dto/update-check-raw-material-detail.dto';

@Controller('check-raw-material-detail')
export class CheckRawMaterialDetailController {
  constructor(
    private readonly checkRawMaterialDetailService: CheckRawMaterialDetailService,
  ) {}

  @Post()
  create(
    @Body() createCheckRawMaterialDetailDto: CreateCheckRawMaterialDetailDto,
  ) {
    return this.checkRawMaterialDetailService.create(
      createCheckRawMaterialDetailDto,
    );
  }

  @Get()
  findAll() {
    return this.checkRawMaterialDetailService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkRawMaterialDetailService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCheckRawMaterialDetailDto: UpdateCheckRawMaterialDetailDto,
  ) {
    return this.checkRawMaterialDetailService.update(
      +id,
      updateCheckRawMaterialDetailDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkRawMaterialDetailService.remove(+id);
  }
}
