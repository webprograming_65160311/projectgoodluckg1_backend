import { CheckStock } from 'src/check-stock/entities/check-stock.entity';
import { RawMaterial } from 'src/raw-material/entities/raw-material.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class CheckRawMaterialDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  bfCheck: number;

  @Column()
  atCheck: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(
    () => RawMaterial,
    (rawMaterial) => rawMaterial.checkRawMaterialDetails,
    { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
  )
  rawMaterial: RawMaterial;

  @ManyToOne(
    () => CheckStock,
    (checkStock) => checkStock.checkRawMaterialDetails,
    { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
  )
  checkStock: CheckStock;
}
