import { Injectable } from '@nestjs/common';
import { CreateCheckRawMaterialDetailDto } from './dto/create-check-raw-material-detail.dto';
import { UpdateCheckRawMaterialDetailDto } from './dto/update-check-raw-material-detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CheckRawMaterialDetail } from './entities/check-raw-material-detail.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CheckRawMaterialDetailService {
  constructor(
    @InjectRepository(CheckRawMaterialDetail)
    private checkStcoksRepository: Repository<CheckRawMaterialDetail>,
  ) {}

  create(createCheckRawMaterialDetailDto: CreateCheckRawMaterialDetailDto) {
    return this.checkStcoksRepository.save(createCheckRawMaterialDetailDto);
  }

  findAll() {
    return this.checkStcoksRepository.find({
      relations: { checkStock: true, rawMaterial: true },
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} checkRawMaterialDetail`;
  }

  update(
    id: number,
    updateCheckRawMaterialDetailDto: UpdateCheckRawMaterialDetailDto,
  ) {
    return `This action updates a #${id} checkRawMaterialDetail`;
  }

  remove(id: number) {
    return `This action removes a #${id} checkRawMaterialDetail`;
  }
}
