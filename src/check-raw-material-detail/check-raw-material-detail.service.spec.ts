import { Test, TestingModule } from '@nestjs/testing';
import { CheckRawMaterialDetailService } from './check-raw-material-detail.service';

describe('CheckRawMaterialDetailService', () => {
  let service: CheckRawMaterialDetailService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckRawMaterialDetailService],
    }).compile();

    service = module.get<CheckRawMaterialDetailService>(
      CheckRawMaterialDetailService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
