import { Module } from '@nestjs/common';
import { CheckRawMaterialDetailService } from './check-raw-material-detail.service';
import { CheckRawMaterialDetailController } from './check-raw-material-detail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckRawMaterialDetail } from './entities/check-raw-material-detail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CheckRawMaterialDetail])],
  controllers: [CheckRawMaterialDetailController],
  providers: [CheckRawMaterialDetailService],
})
export class CheckRawMaterialDetailModule {}
