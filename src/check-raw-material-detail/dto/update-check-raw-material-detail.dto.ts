import { PartialType } from '@nestjs/swagger';
import { CreateCheckRawMaterialDetailDto } from './create-check-raw-material-detail.dto';

export class UpdateCheckRawMaterialDetailDto extends PartialType(
  CreateCheckRawMaterialDetailDto,
) {}
