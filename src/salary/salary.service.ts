import { Injectable } from '@nestjs/common';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Repository } from 'typeorm';
import { Salary } from './entities/salary.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class SalaryService {
  constructor(
    @InjectRepository(Salary) private salaryRepository: Repository<Salary>,
  ) { }
  create(createSalaryDto: CreateSalaryDto) {
    return this.salaryRepository.save(createSalaryDto);
  }

  findAll() {
    return this.salaryRepository.find({
      relations: ['user', 'user.branch', 'user.role'],
    });
  }

  findOne(id: number) {
    return this.salaryRepository.findOneBy({ id });
  }

  findMySalary(id: number) {
    return this.salaryRepository.find({
      where: { user: { id } },
      relations: { user: true },
      order: { id: 'DESC' },
    });
  }

  update(id: number, updateSalaryDto: UpdateSalaryDto) {
    return `This action updates a #${id} salary`;
  }

  remove(id: number) {
    return `This action removes a #${id} salary`;
  }
}
