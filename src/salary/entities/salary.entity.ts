import { TimeAttendance } from 'src/time-attendance/entities/time-attendance.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true, default: () => "datetime('now')" })
  payMonth: Date;

  @Column({ nullable: true, default: new Date().toISOString().substr(0, 10) })
  payDate: Date;

  @Column()
  paymentType: string;

  // @Column()
  // amount: number;

  @Column()
  deduction: number;

  @Column()
  bonus: number;

  @Column()
  total: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => User, (user) => user.salary, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  user: User;

  @OneToMany(() => TimeAttendance, (ta) => ta.salary)
  timeAttendances: TimeAttendance[];
}
