import { Test, TestingModule } from '@nestjs/testing';
import { TimeAttendanceController } from './time-attendance.controller';
import { TimeAttendanceService } from './time-attendance.service';

describe('TimeAttendanceController', () => {
  let controller: TimeAttendanceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TimeAttendanceController],
      providers: [TimeAttendanceService],
    }).compile();

    controller = module.get<TimeAttendanceController>(TimeAttendanceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
