import { Test, TestingModule } from '@nestjs/testing';
import { TimeAttendanceService } from './time-attendance.service';

describe('TimeAttendanceService', () => {
  let service: TimeAttendanceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TimeAttendanceService],
    }).compile();

    service = module.get<TimeAttendanceService>(TimeAttendanceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
