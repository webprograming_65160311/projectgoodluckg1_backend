import { Injectable } from '@nestjs/common';
import { CreateTimeAttendanceDto } from './dto/create-time-attendance.dto';
import { UpdateTimeAttendanceDto } from './dto/update-time-attendance.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { TimeAttendance } from './entities/time-attendance.entity';
import { Repository } from 'typeorm';
import { Branch } from 'src/branches/entities/branch.entity';

@Injectable()
export class TimeAttendanceService {
  constructor(
    @InjectRepository(TimeAttendance)
    private timeAttendanceRepository: Repository<TimeAttendance>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
  ) {}

  async create(createTimeAttendanceDto: CreateTimeAttendanceDto) {
    const checkIn = await this.timeAttendanceRepository.save(
      createTimeAttendanceDto,
    );
    console.log(checkIn);
    return checkIn;
  }

  findAll() {
    return this.timeAttendanceRepository.find({
      relations: { user: { branch: true } },
      order: { id: 'DESC' },
    });
  }

  findMyCheck(id: number) {
    return this.timeAttendanceRepository.find({
      where: { user: { id } },
      relations: { user: { branch: true } },
      order: { id: 'DESC' },
    });
  }

  async findOne(id: number) {
    return await this.timeAttendanceRepository.findOneOrFail({
      relations: { user: true },
      where: { id },
    });
  }

  async findOneByBranch(id: number) {
    const branch = await this.branchRepository.findOne({ where: { id } });
    return await this.timeAttendanceRepository.find({
      relations: { user: { branch: true } },
      where: { user: { branch: { id: branch.id } } },
    });
  }

  async update(id: number, updateTimeAttendanceDto: UpdateTimeAttendanceDto) {
    await this.timeAttendanceRepository.findOneByOrFail({ id });
    await this.timeAttendanceRepository.update(id, updateTimeAttendanceDto);
    const update = await this.timeAttendanceRepository.findOne({
      relations: { user: true },
      where: { id },
    });
    return update;
  }

  async remove(id: number) {
    const del = await this.timeAttendanceRepository.findOneOrFail({
      where: { id },
    });
    return this.timeAttendanceRepository.remove(del);
  }
}
