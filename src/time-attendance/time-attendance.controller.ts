import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { TimeAttendanceService } from './time-attendance.service';
import { CreateTimeAttendanceDto } from './dto/create-time-attendance.dto';
import { UpdateTimeAttendanceDto } from './dto/update-time-attendance.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@UseGuards(AuthGuard)
@Controller('time-attendance')
export class TimeAttendanceController {
  constructor(private readonly timeAttendanceService: TimeAttendanceService) {}

  @Post()
  create(@Body() createTimeAttendanceDto: CreateTimeAttendanceDto) {
    return this.timeAttendanceService.create(createTimeAttendanceDto);
  }

  @Get()
  findAll() {
    return this.timeAttendanceService.findAll();
  }

  @Get('my-own/:id')
  findMyCheck(@Param('id') id: string) {
    return this.timeAttendanceService.findMyCheck(+id);
  }

  @Get('branch/:id')
  findByBranch(@Param('id') id: string) {
    return this.timeAttendanceService.findOneByBranch(+id);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.timeAttendanceService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTimeAttendanceDto: UpdateTimeAttendanceDto,
  ) {
    return this.timeAttendanceService.update(+id, updateTimeAttendanceDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.timeAttendanceService.remove(+id);
  }
}
