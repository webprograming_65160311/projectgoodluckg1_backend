import { Module } from '@nestjs/common';
import { TimeAttendanceService } from './time-attendance.service';
import { TimeAttendanceController } from './time-attendance.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TimeAttendance } from './entities/time-attendance.entity';
import { Branch } from 'src/branches/entities/branch.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TimeAttendance, Branch])],
  controllers: [TimeAttendanceController],
  providers: [TimeAttendanceService],
  exports: [TimeAttendanceService],
})
export class TimeAttendanceModule {}
