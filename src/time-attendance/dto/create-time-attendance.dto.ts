import { User } from 'src/users/entities/user.entity';

export class CreateTimeAttendanceDto {
  checkIn: Date;
  checkOut?: Date;
  status: string; //ทันเวลา  มาสาย
  hour?: number;
  user: User;
}
