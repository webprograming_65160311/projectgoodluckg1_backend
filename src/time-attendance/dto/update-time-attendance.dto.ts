import { PartialType } from '@nestjs/swagger';
import { CreateTimeAttendanceDto } from './create-time-attendance.dto';

export class UpdateTimeAttendanceDto extends PartialType(
  CreateTimeAttendanceDto,
) {}
