import { Injectable } from '@nestjs/common';
import { CreateRawMaterialDto } from './dto/create-raw-material.dto';
import { UpdateRawMaterialDto } from './dto/update-raw-material.dto';
import { RawMaterial } from './entities/raw-material.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RawMaterialService {
  constructor(
    @InjectRepository(RawMaterial)
    private rawMaterialsRepository: Repository<RawMaterial>,
  ) {}
  create(createRawMaterialDto: CreateRawMaterialDto) {
    const rawMaterial = new RawMaterial();
    rawMaterial.name = createRawMaterialDto.name;

    rawMaterial.unit = createRawMaterialDto.unit;

    if (createRawMaterialDto.image && createRawMaterialDto.image != '')
      rawMaterial.image = createRawMaterialDto.image;
    return this.rawMaterialsRepository.save(rawMaterial);
  }

  findAll(): Promise<RawMaterial[]> {
    return this.rawMaterialsRepository.find({
      relations: ['stocks', 'stocks.branch'],
    });
  }

  findOne(id: number) {
    return this.rawMaterialsRepository.findOneByOrFail({ id });
  }

  async getLastId(): Promise<number | undefined> {
    const [latestRaw] = await this.rawMaterialsRepository.find({
      order: { id: 'DESC' },
      take: 1,
    });
    return latestRaw?.id;
  }

  async update(id: number, updateRawMaterialDto: UpdateRawMaterialDto) {
    const rawMaterial = await this.rawMaterialsRepository.findOneOrFail({
      where: { id },
    });
    rawMaterial.name = updateRawMaterialDto.name;

    rawMaterial.unit = updateRawMaterialDto.unit;

    if (updateRawMaterialDto.image && updateRawMaterialDto.image != '') {
      rawMaterial.image = updateRawMaterialDto.image;
    }
    await this.rawMaterialsRepository.save(rawMaterial);

    const result = await this.rawMaterialsRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const delRawMaterial = await this.rawMaterialsRepository.findOneOrFail({
      where: { id },
    });

    await this.rawMaterialsRepository.remove(delRawMaterial);

    return delRawMaterial;
  }
}
