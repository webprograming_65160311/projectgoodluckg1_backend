export class CreateRawMaterialDto {
  name: string;
  quantity: string;
  price: string;
  minimum: string;
  unit: string;
  status: string;
  image: string;
}
