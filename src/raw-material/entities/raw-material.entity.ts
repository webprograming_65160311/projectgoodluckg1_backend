import { Branch } from 'src/branches/entities/branch.entity';
import { CheckRawMaterialDetail } from 'src/check-raw-material-detail/entities/check-raw-material-detail.entity';
import { Stock } from 'src/stock/entities/stock.entity';

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class RawMaterial {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: '' })
  name: string;

  @Column()
  unit: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => CheckRawMaterialDetail, (detail) => detail.checkStock)
  checkRawMaterialDetails: CheckRawMaterialDetail[];

  @OneToMany(() => Stock, (stock) => stock.rawMaterial)
  stocks: Stock[];
}
