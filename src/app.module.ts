import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { RawMaterialModule } from './raw-material/raw-material.module';
import { CheckStockModule } from './check-stock/check-stock.module';
import { CheckRawMaterialDetailModule } from './check-raw-material-detail/check-raw-material-detail.module';
import { RawMaterial } from './raw-material/entities/raw-material.entity';
import { CheckStock } from './check-stock/entities/check-stock.entity';
import { CheckRawMaterialDetail } from './check-raw-material-detail/entities/check-raw-material-detail.entity';
import { UsersModule } from './users/users.module';
import { RolesModule } from './roles/roles.module';
import { ReceiptsModule } from './receipts/receipts.module';
import { PromotionsModule } from './promotions/promotions.module';
import { MembersModule } from './members/members.module';
import { ProductsModule } from './products/products.module';
import { SizesModule } from './sizes/sizes.module';
import { TypesModule } from './types/types.module';
import { CategoriesModule } from './categories/categories.module';
import { User } from './users/entities/user.entity';
import { Role } from './roles/entities/role.entity';
import { Receipt } from './receipts/entities/receipt.entity';
import { ReceiptItem } from './receipts/entities/receiptItem.entity';
import { Product } from './products/entities/product.entity';
import { Category } from './categories/entities/category.entity';
import { Size } from './sizes/entities/size.entity';
import { Type } from './types/entities/type.entity';
import { Promotion } from './promotions/entities/promotion.entity';
import { Member } from './members/entities/member.entity';
import { AuthModule } from './auth/auth.module';
import { join } from 'path';
import { TimeAttendance } from './time-attendance/entities/time-attendance.entity';
import { TimeAttendanceModule } from './time-attendance/time-attendance.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { OrderBillModule } from './order-bill/order-bill.module';
import { OrderBillDetailModule } from './order-bill-detail/order-bill-detail.module';
import { OrderBill } from './order-bill/entities/order-bill.entity';
import { OrderBillDetail } from './order-bill-detail/entities/order-bill-detail.entity';
import { Salary } from './salary/entities/salary.entity';
import { SalaryModule } from './salary/salary.module';
import { UtilityBillModule } from './utility-bill/utility-bill.module';
import { UtilityBill } from './utility-bill/entities/utility-bill.entity';
import { TypeUtilityBillModule } from './type-utility-bill/type-utility-bill.module';
import { TypeUtilityBill } from './type-utility-bill/entities/type-utility-bill.entity';
import { PromotionTypesModule } from './promotion-types/promotion-types.module';
import { PromotionType } from './promotion-types/entities/promotion-type.entity';
import { BranchesModule } from './branches/branches.module';
import { Branch } from './branches/entities/branch.entity';
import { StockModule } from './stock/stock.module';
import { Stock } from './stock/entities/stock.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.sqlite',
      synchronize: true,
      entities: [
        Branch,
        RawMaterial,
        CheckStock,
        CheckRawMaterialDetail,
        User,
        Role,
        Receipt,
        ReceiptItem,
        Product,
        Category,
        Type,
        Size,
        Salary,
        Promotion,
        PromotionType,
        Member,
        TimeAttendance,
        OrderBill,
        OrderBillDetail,
        UtilityBill,
        TypeUtilityBill,
        Stock,
      ],
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    RawMaterialModule,
    CheckStockModule,
    CheckRawMaterialDetailModule,
    UsersModule,
    RolesModule,
    ReceiptsModule,
    PromotionsModule,
    MembersModule,
    ProductsModule,
    SizesModule,
    TypesModule,
    CategoriesModule,
    AuthModule,
    TimeAttendanceModule,
    OrderBillModule,
    OrderBillDetailModule,
    SalaryModule,
    UtilityBillModule,
    TypeUtilityBillModule,
    PromotionTypesModule,
    BranchesModule,
    StockModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
