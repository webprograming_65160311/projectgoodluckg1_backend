import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Stock } from './entities/stock.entity';
import { Repository } from 'typeorm';

@Injectable()
export class StockService {
  constructor(
    @InjectRepository(Stock)
    private stockRepository: Repository<Stock>,
  ) {}
  create(createStock: CreateStockDto) {
    return this.stockRepository.save(createStock);
  }

  findAll() {
    return this.stockRepository.find({
      relations: { branch: true, rawMaterial: true },
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} stock`;
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    await this.stockRepository.findOneByOrFail({ id });
    await this.stockRepository.update(id, updateStockDto);
    const update = await this.stockRepository.findOne({
      relations: { rawMaterial: true, branch: true },
      where: { id },
    });
    return update;
  }

  remove(id: number) {
    return `This action removes a #${id} stock`;
  }
}
