import { Branch } from 'src/branches/entities/branch.entity';
import { RawMaterial } from 'src/raw-material/entities/raw-material.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  quantity: number;

  @Column()
  price: number;

  @Column()
  minimum: number;

  @Column()
  status: string;

  @ManyToOne(() => RawMaterial, (rawMaterial) => rawMaterial.stocks, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  rawMaterial: RawMaterial;

  @ManyToOne(() => Branch, (branch) => branch.stocks, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  branch: Branch;
}
